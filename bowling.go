package bowling

const SKIP = -1 // skip second roll of frame if first one is a strike
const stdRollCount = 20

func calcBonus(i int, rolls []int) int {
    bonusSum := 0

    // strike
    if rolls[i] == 10 {
        bonusSum += rolls[i+2]
        // correct strike calculation if next frame is also a strike
        if rolls[i+3] != SKIP {
            bonusSum += rolls[i+3]
        } else {
            bonusSum += rolls[i+4]
        }

    // spare
    } else if rolls[i] + rolls[i+1] == 10 {
        bonusSum += rolls[i+2]
    }

    return bonusSum
}

func evalLine(rolls []int) int {
    sum := 0

    for i := 0; i < stdRollCount; i++ {
        if rolls[i] == SKIP {
            continue
        }

        sum += rolls[i];

        // detect frame start
        if i % 2 == 0 {
            sum += calcBonus(i, rolls)
        }
    }

    return sum
}
