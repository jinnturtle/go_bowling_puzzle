package bowling

import (
    "testing"

    "github.com/stretchr/testify/assert"
)

// full line and a couple spares at the end
func Test1(t *testing.T) {
    rolls := []int{
        10, SKIP, // strike: 10 + 1 + 2 = 13
        1, 2,
        2, 2, // 20
        3, 7, // spare: 10 + 2 = 12; +18 = 32
        2, 2,
        3, 3, // 42
        7, 3, // spare: 10 + 8 = 18; +40 = 60
        8, 1, // 69
        5, 5, // spare: 10 + 5 = 15; +69 = 84
        5, 5, // spare: 10 + 5 = 15; +84 = 99
        5,    // extra roll because of spare on last frame
    }

    expected := 99
    assert.Equal(t, expected, evalLine(rolls))
}

// 10 pins on last roll
func Test2(t *testing.T) {
    rolls := []int{
        10, SKIP, // strike: 10 + 1 + 1 = 12
        1, 1,
        2, 2, // 18
        3, 7, // spare: 10 + 2 = 12; +18 = 30
        2, 2,
        3, 3, // 40
        7, 3, // spare: 10 + 8 = 18; +40 = 58
        8, 1, // 67
        5, 5, // spare: 10 + 5 = 15; +67 = 82
        5, 5, // spare: 10 + 10 = 20; +82 = 102
        10,   // extra roll because of spare on last frame
    }

    expected := 102
    assert.Equal(t, expected, evalLine(rolls))
}

// no bonus roll at the end
func Test3(t *testing.T) {
    rolls := []int{
        10, SKIP, // strike: 10 + 1 + 1 = 12
        1, 1,
        2, 2, // 18
        3, 7, // spare: 10 + 2 = 12; +18 = 30
        2, 2,
        3, 3, // 40
        7, 3, // spare: 10 + 8 = 18; +40 = 58
        8, 1, // 67
        5, 5, // spare: 10 + 5 = 15; +67 = 82
        5, 4, //
    }

    expected := 91
    assert.Equal(t, expected, evalLine(rolls))
}

// all strikes
func Test4(t *testing.T) {
    rolls := []int{
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, SKIP, // strike: 10 + 10 + 10 = 30
        10, 10,   // bonus frame not counted as strike so no skip
    }

    expected := 300
    assert.Equal(t, expected, evalLine(rolls))
}

// all nines
func Test5(t *testing.T) {
    rolls := []int{
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
        9, 0,
    }

    expected := 90
    assert.Equal(t, expected, evalLine(rolls))
}

// all spares
func Test6(t *testing.T) {
    rolls := []int{
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5, 5,
        5,    // bonus roll because of previous spare
    }

    expected := 150
    assert.Equal(t, expected, evalLine(rolls))
}
